package itis.parsing2;

import itis.parsing2.FactoryParsingException.FactoryValidationError;
import itis.parsing2.annotations.Concatenate;
import itis.parsing2.annotations.NotBlank;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class FactoryParsingServiceImpl implements FactoryParsingService {

    @Override
    public Factory parseFactoryData(String factoryDataDirectoryPath) throws FactoryParsingException {

        BufferedReader bufferedReader;

        List<FactoryValidationError> validationErrors = new ArrayList<>();

        List<String> keys = new ArrayList<>();
        List<String> values = new ArrayList<>();

        try {
            bufferedReader = new BufferedReader(new FileReader(new File(factoryDataDirectoryPath)));

            while (bufferedReader.ready()) {

                String line = bufferedReader.readLine();

                if (line.equals("---")) {
                    continue;
                }

                String[] string = line.split(":");

                keys.add(string[0].substring(1, string[0].length() - 1));

                string[1] = string[1].trim().replaceAll("\"", "");
                values.add(string[1]);

            }
        } catch (IOException e) {
            throw new IllegalAccessError("Неправильнйы путь или нет файла");
        }

        Factory factory = null;
        Class<?> classFactory = Factory.class;

        try {

            Constructor[] constructors = classFactory.getDeclaredConstructors();
            Constructor constructor = constructors[0];

            factory = (Factory) constructor.newInstance();

            Field[] fields = classFactory.getDeclaredFields();

            for (Field field : fields) {

                field.setAccessible(true);

                Annotation[] annotations = field.getAnnotations();

                boolean thisFieldNotBlank = false, thisFieldConcatenate = false;
                String[] stringNameField;
                String delimiter = "";

                if (field.getAnnotation(NotBlank.class) != null) {
                    thisFieldNotBlank = true;
                }

                if (field.getAnnotation(Concatenate.class) != null) {
                    thisFieldConcatenate = true;
                    Concatenate concatenate = field.getAnnotation(Concatenate.class);

                    delimiter = concatenate.delimiter();
                    stringNameField = concatenate.fieldNames();
                } else {
                    stringNameField = new String[1];
                    stringNameField[0] = field.getName();
                }

                String parametr = "";
                int countParametr = 0;
                boolean flag = false;

                for (String str : stringNameField) {

                    for (int i = 0; i < keys.size(); i++) {
                        String key = keys.get(i);

                        if (str.equals(key)) {
                            String value = values.get(i);
                            if ((value.equals("null") || value.equals("")) && thisFieldNotBlank) {

                                validationErrors.add(new FactoryValidationError(field.getName(), "NotBlank"));

                                flag = true;
                            } else {

                                if (field.getType() == Long.class) {
                                    field.set(factory, Long.parseLong(value));
                                    flag = true;
                                } else {
                                    countParametr++;
                                    if (parametr.length() == 0) {
                                        parametr += (value);
                                    } else {
                                        parametr += (delimiter);
                                        parametr += (value);
                                    }

                                    if (stringNameField.length == 1) {
                                        field.set(factory, value);
                                    }

                                }
                            }

                            if (flag) {
                                break;
                            }

                        }
                    }

                    if (flag) {
                        break;
                    }

                }

                if (!flag) {
                    if (thisFieldConcatenate) {

                        if (countParametr == stringNameField.length) {
                            field.set(factory, parametr);
                        } else {

                            for (String str : stringNameField) {
                                boolean t = false;
                                for (int i = 0; i < keys.size(); i++) {
                                    if (str.equals(keys.get(i))) {
                                        t = true;
                                    }
                                }
                                if (!t) {
                                    validationErrors.add(new FactoryValidationError(str, "Не существует"));
                                }
                            }

                        }
                    }
                }

                if (field.getAnnotation(NotBlank.class) != null) {
                    if (field.get(factory) == null) {
                        validationErrors.add(new FactoryValidationError(field.getName(), "NotBlank"));
                    }
                }

            }


        } catch (Exception e) {

        }

        if (validationErrors.size() > 0) {
            throw new IllegalArgumentException(
                    new FactoryParsingException("не соблюдаются ограничения аннотаций", validationErrors));
        }

        return factory;
    }
}
